<?php
add_action( 'elementor_pro/init', function() {
	// Here its safe to include our action class file
	include_once( 'path/to/action/class/file' );
    /**
     * Class VPBTCH_formElementor
     * @see https://developers.elementor.com/custom-form-action/
     * Custom elementor form action after submit to add a subsciber to
     * Sendy list via API 
     */
    class VPBTCH_formElementor extends \ElementorPro\Modules\Forms\Classes\Action_Base {
        /**
         * Get Name
         *
         * Return the action name
         *
         * @access public
         * @return string
         */
        public function get_name() {
            return 'VPBTCH';
        }

        /**
         * Get Label
         *
         * Returns the action label
         *
         * @access public
         * @return string
         */
        public function get_label() {
            return __( 'VPBTCH', 'text-domain' );
        }

        /**
         * Run
         *
         * Runs the action after submit
         *
         * @access public
         * @param \ElementorPro\Modules\Forms\Classes\Form_Record $record
         * @param \ElementorPro\Modules\Forms\Classes\Ajax_Handler $ajax_handler
         */
        public function run( $record, $ajax_handler ) {
            $settings = $record->get( 'form_settings' );
            if ( empty( $settings['sendy_url'] ) ) {
                return;
            }

            $raw_fields = $record->get( 'fields' );

            $fields = [];
            foreach ( $raw_fields as $id => $field ) {
                $fields[ $id ] = $field['value'];
            }

            if ( empty( $fields[ $settings['pasword'] ] ) ) {
                return;
            }
        }

        /**
         * Register Settings Section
         *
         * Registers the Action controls
         *
         * @access public
         * @param \Elementor\Widget_Base $widget
         */
        public function register_settings_section( $widget ) {
            $widget->start_controls_section(
                'section_sendy',
                [
                    'label' => __( 'Sendy', 'text-domain' ),
                    'condition' => [
                        'submit_actions' => $this->get_name(),
                    ],
                ]
            );

            $widget->add_control(
                'sendy_url',
                [
                    'label' => __( 'Sendy URL', 'text-domain' ),
                    'type' => \Elementor\Controls_Manager::TEXT,
                    'placeholder' => 'http://your_sendy_installation/',
                    'label_block' => true,
                    'separator' => 'before',
                    'description' => __( 'Enter the URL where you have Sendy Password', 'text-domain' ),
                ]
            );


            $widget->end_controls_section();

        }

        /**
         * On Export
         *
         * Clears form settings on export
         * @access Public
         * @param array $element
         */
        public function on_export( $element ) {
            unset(
                $element['sendy_url'],
            );
        }
    }
	// Instantiate the action class
	$sendy_action = new VPBTCH_formElementor();

	// Register the action with form widget
	\ElementorPro\Plugin::instance()->modules_manager->get_modules( 'forms' )->add_form_action( $sendy_action->get_name(), $sendy_action );
});