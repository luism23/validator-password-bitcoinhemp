<?php
function VPBTCH_create_menu() {

	//create new top-level menu
	add_menu_page('VPBTCH Settings', 'VPBTCH', 'administrator', _FILE_, 'VPBTCH_settings_page'  );

	//call register settings function
	add_action( 'admin_init', 'register_VPBTCH_settings' );
}
add_action('admin_menu', 'VPBTCH_create_menu');


function register_VPBTCH_settings() {
	//register our settings
	register_setting( 'VPBTCH-settings-group', 'new_option_name' );
	register_setting( 'VPBTCH-settings-group', 'some_other_option' );
	register_setting( 'VPBTCH-settings-group', 'option_etc' );
}

function VPBTCH_settings_page() {
    if($_POST["VPBTCH_save"] == "Save"){
        update_option("VPBTCH_password",$_POST["VPBTCH_password"]);
        update_option("VPBTCH_urlRedirect",$_POST["VPBTCH_urlRedirect"]);
    }
    ?>
    <div class="wrap">
        <h1>
            Validador de Password
        </h1>
        <form method="post">
            <input type="text" name="VPBTCH_urlRedirect" id="VPBTCH_urlRedirect" 
            value="<?=get_option("VPBTCH_urlRedirect")?>" placeholder="Url Redirect not password" />
            <br>
            <input type="password" name="VPBTCH_password" id="VPBTCH_password" 
            value="<?=get_option("VPBTCH_password")?>" placeholder="Password"/>
            <br>
            <input type="submit" class="button" value="Save" name="VPBTCH_save"  id="VPBTCH_save">
        </form>
    </div>
    <?php 
}