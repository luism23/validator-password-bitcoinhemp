<?php
/*
Plugin Name: Validator Password BTCH
Plugin URI: https://gitlab.com/luism23/validator-password-bitcoinhemp
Description: Add shorcode for validate password BTCH.
Author: LuisMiguelNarvaez
Version: 1.0.13
Author URI: https://gitlab.com/luism23
License: GPL
 */

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/luism23/validator-password-bitcoinhemp',
	__FILE__,
	'validator-password-bitcoinhemp'
);
$myUpdateChecker->setAuthentication('glpat-puSVzsHaWSc66qa_rVr1');
$myUpdateChecker->setBranch('master');


define("VPBTCH_DIR",plugin_dir_path( __FILE__ ));
define("VPBTCH_URL",plugin_dir_url( __FILE__ ));

require_once  VPBTCH_DIR . 'src/index.php';